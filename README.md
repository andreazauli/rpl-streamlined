# rpl-streamlined

rpl tool fork. Added stdin/stdout streams management for in line management.

Example::
```
   zcat myfile.gz | rpl "oldstring" "newstring" - | gzip  > mynewfile.gz 
   zcat myfile.gz | rpl "oldstring" "newstring" - | rpl "oldstring2" "newstring2" - | gzip  > mynewfile.gz 
```
